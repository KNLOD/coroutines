import kotlinx.coroutines.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


val time = { DateTimeFormatter.ISO_LOCAL_TIME.format(LocalDateTime.now()) }


/**
Suspend function getValueAsync returns random number to be used Concurrently
@counter number of value
 */
suspend fun getValueAsync(counter : Int) : Double{

    println("I'm sleeping but async ($counter): time: ${time()}")
    delay(1000)

    return Math.random()
}

/**
general function getValue, returns random number to be used Consequentially
 @counter number of value
 */
fun getValue(counter : Int): Double{

    println("I'm sleeping not async ($counter): time: ${time()}")
    Thread.sleep(1000)

    return Math.random()
}


fun main(){

    /**
    Consequential block of code
     */

    val started = System.currentTimeMillis()

    // first, second - values that will be added to each other
    val first = getValue(1)
    val second = getValue(2)

    println("I'm Tired of waitng! I'm running finally main:\n" +
            " Result:  ${first + second}")
    println("Now I can quit, execution time: ${(System.currentTimeMillis() - started)/1000} seconds")
    println()

    /**
    Concurrent block of code
     */
    runBlocking {
        val started = System.currentTimeMillis()
        val first = async { getValueAsync(1) }
        val second = async { getValueAsync(2) }

        println("I'm Tired of waitng! I'm running finally main:"+
                "\n Result: ${first.await() + second.await()}")
        println("Now I can quit, execution time: ${(System.currentTimeMillis() - started)/1000} seconds")
    }
}
